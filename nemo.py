import cv2

original = cv2.VideoCapture('/home/iggodinho/vision/nemo.mp4')
i=True

while i:
    aux,frame = original.read()
    if aux:
	change = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	cv2.imshow('Original',frame)
	orangeL = (1,190,200)
        orangeD = (18,255,255)
        white = (0,0,200)
        gray = (145,60,255)
	colours=(orangeL,orangeD,white,gray)
        mask= cv2.inRange(change, colours[0], colours[1]) + cv2.inRange(change,colours[2],colours[3]) 
        edit = cv2.bitwise_and(frame, frame, mask=mask)
        cv2.imshow('Edited',edit)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            i=False
    else: 
	i=False
